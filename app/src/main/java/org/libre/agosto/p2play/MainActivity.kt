package org.libre.agosto.p2play

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import org.libre.agosto.p2play.adapters.VideosAdapter
import org.libre.agosto.p2play.ajax.Videos
import org.libre.agosto.p2play.models.VideoModel

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    val client: Videos = Videos()
    private lateinit var lastItem: MenuItem
    lateinit var myMenu: Menu
    val _db = Database(this)
    var section: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        /* fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        } */

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        // Context for ManagerSingleton
        ManagerSingleton.context = this

        nav_view.setNavigationItemSelectedListener(this)
        viewManager = LinearLayoutManager(this)

        // Set data for RecyclerView
        this.setData(arrayListOf())

        this.getLastVideos()

        swipeContainer.setOnRefreshListener {
            this.refresh()
        }
    }

    // Generic function for set data to RecyclerView
    fun setData(data:ArrayList<VideoModel>){
        viewAdapter = VideosAdapter(data)

        recyclerView = findViewById<RecyclerView>(R.id.list).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
        swipeContainer.isRefreshing = false
    }

    fun refresh(){
        swipeContainer.isRefreshing = true
        when(section){
            "local" -> this.getLocalVideos()
            "popular" -> this.getPopularVideos()
            "last" -> this.getLastVideos()
            "my_videos" -> {
                if(ManagerSingleton.token.token != "")
                    this.getMyVideos()
                else
                    this.getLastVideos()
            }
        }
    }

    // Last videos
    fun getLastVideos(){
        swipeContainer.isRefreshing = true
        section = "last"
        setTitle(R.string.title_recent)
        AsyncTask.execute {
            val videos = client.getLastVideos()
            runOnUiThread {
                this.setData(videos)
            }
        }
    }

    //
    fun getPopularVideos(){
        swipeContainer.isRefreshing = true
        section = "popular"
        setTitle(R.string.title_popular)
        AsyncTask.execute {
            val videos = client.getPopularVideos()
            runOnUiThread {
                this.setData(videos)
            }
        }
    }

    fun getLocalVideos(){
        swipeContainer.isRefreshing = true
        section = "local"
        setTitle(R.string.title_local)
        AsyncTask.execute {
            val videos = client.getLocalVideos()
            runOnUiThread {
                this.setData(videos)
            }
        }
    }

    fun getMyVideos(){
        swipeContainer.isRefreshing = true
        section = "my_videos"
        setTitle(R.string.title_myVideos)
        AsyncTask.execute {
            val videos = client.myVideos(ManagerSingleton.token.token)
            runOnUiThread {
                this.setData(videos)
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        myMenu = menu
        setSideData()
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        myMenu = menu!!

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_login -> {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_logout -> {
                logout()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        // if(::lastItem.isInitialized){
            // lastItem.isChecked = false
        // }
        lastItem = item
        // item.isChecked = true
        when (item.itemId) {
            R.id.nav_popular-> {
                getPopularVideos()
            }
            R.id.nav_recent-> {
                getLastVideos()
            }
            R.id.nav_local-> {
                getLocalVideos()
            }
            R.id.nav_about-> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        super.onResume()
        ManagerSingleton.context = this
        setSideData()
    }

    private fun setSideData(){
        if(ManagerSingleton.user.status == 1){
            side_usernameTxt?.text = ManagerSingleton.user.username
            side_emailTxt?.text = ManagerSingleton.user.email
            if(ManagerSingleton.user.avatar!="" && side_imageView != null)
                Picasso.get().load("https://"+ManagerSingleton.url+ManagerSingleton.user.avatar).into(side_imageView)
            side_imageView?.setOnClickListener {
                getMyVideos()
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            if(::myMenu.isInitialized){
                myMenu.findItem(R.id.action_login).isVisible = false
                myMenu.findItem(R.id.action_logout).isVisible = true
            }
        }
    }

    fun logout(){
        if(::myMenu.isInitialized){
            myMenu.findItem(R.id.action_login).isVisible = true
            myMenu.findItem(R.id.action_logout).isVisible = false
        }

        side_usernameTxt?.text = getString(R.string.nav_header_title)
        side_emailTxt?.text = getString(R.string.nav_header_subtitle)
        side_imageView?.setImageResource(R.mipmap.ic_launcher_round)
        side_imageView?.setOnClickListener {  }
        _db.logout()
        ManagerSingleton.logout()

        this.refresh()
        ManagerSingleton.Toast(getString(R.string.logout_msg))

    }


}

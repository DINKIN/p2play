package org.libre.agosto.p2play.models

import java.io.Serializable

class VideoModel(
        var name:String="",
        var description:String="",
        var thumbUrl:String="",
        var userImageUrl:String="",
        var embedUrl:String="",
        var duration:Number=0,
        var username:String="",
        var views:Number=0
        ):Serializable
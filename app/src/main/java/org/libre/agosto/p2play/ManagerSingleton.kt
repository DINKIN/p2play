package org.libre.agosto.p2play

import android.content.Context
import org.libre.agosto.p2play.models.TokenModel
import org.libre.agosto.p2play.models.UserModel

object ManagerSingleton {
    var context: Context?= null
    var url: String?= null
    var user: UserModel = UserModel()
    var token: TokenModel = TokenModel()
    // var keys:

    fun Toast(text: String?) {
        if(this.context == null) { return }
        android.widget.Toast.makeText(this.context, text, android.widget.Toast.LENGTH_SHORT).show()
    }

    fun logout(){
        user = UserModel()
        token = TokenModel()
    }
}
package org.libre.agosto.p2play.ajax

import android.util.JsonReader
import android.util.JsonToken
import android.util.Log
import org.libre.agosto.p2play.models.VideoModel
import java.io.InputStreamReader

class Videos: Client() {

    fun parseVideos(data: JsonReader): ArrayList<VideoModel>{
        var videos = arrayListOf<VideoModel>()
        data.beginObject()
        while (data.hasNext()){
            when(data.nextName()){
                "data"->{
                    data.beginArray()
                    while (data.hasNext()) {
                        val video = VideoModel()
                        data.beginObject()
                        while (data.hasNext()){
                            val key = data.nextName()
                            when (key.toString()) {
                                "name"->{
                                    video.name= data.nextString()
                                }
                                "description"->{
                                    if(data.peek() == JsonToken.STRING)
                                        video.description = data.nextString()
                                    else
                                        data.skipValue()
                                }
                                "duration"->{
                                    video.duration = data.nextInt()
                                }
                                "thumbnailPath"->{
                                    video.thumbUrl = data.nextString()
                                }
                                "embedPath"->{
                                    video.embedUrl = data.nextString()
                                }
                                "views"->{
                                    video.views = data.nextInt()
                                }
                                "account"->{
                                    data.beginObject()
                                    while (data.hasNext()){
                                        val acKey = data.nextName()
                                        when(acKey.toString()){
                                            "name"->video.username=data.nextString()
                                            "avatar"->{
                                                if(data.peek() == JsonToken.BEGIN_OBJECT){
                                                    data.beginObject()
                                                    while (data.hasNext()){
                                                        val avKey = data.nextName()
                                                        when(avKey){
                                                            "path"-> video.userImageUrl = data.nextString()
                                                            else-> data.skipValue()
                                                        }
                                                    }
                                                    data.endObject()
                                                }
                                                else
                                                    data.skipValue()

                                            }
                                            else-> data.skipValue()
                                        }
                                    }
                                    data.endObject()
                                }
                                else->{
                                    data.skipValue()
                                }
                            }
                        }
                        data.endObject()
                        videos.add(video)
                    }
                    data.endArray()
                }
                else-> data.skipValue()
            }
        }
        data.endObject()

        return videos
    }

    private fun getVideos(start:Int, count:Int, sort:String = "-publishedAt", filter:String = ""):ArrayList<VideoModel>{
        val params = "start=$start&count=$count&sort=$sort&filter=$filter"
        var con=this._newCon("videos?$params","GET")
        var videos = arrayListOf<VideoModel>()
        try {
            if (con.responseCode == 200) {
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                videos = parseVideos(data)
            }
        } catch(err:Exception){
            err?.printStackTrace()
            Log.d("TypeErr",err?.message ,err.cause)
            Log.d("Error","fallo la coneccion")
        }
        return videos
    }

    fun getLastVideos(start:Int = 0, count:Int = 30): ArrayList<VideoModel>{
        return this.getVideos(start, count)
    }

    fun getPopularVideos(start:Int = 0, count:Int = 30): ArrayList<VideoModel>{
        return this.getVideos(start, count,"-views")
    }

    fun getLocalVideos(start:Int = 0, count:Int = 30): ArrayList<VideoModel>{
        return this.getVideos(start, count,"-publishedAt", "local")
    }

    fun myVideos(token: String): ArrayList<VideoModel>{
        var con=this._newCon("users/me/videos","GET", token)
        var videos = arrayListOf<VideoModel>()
        try {
            if (con.responseCode == 200) {
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                videos = parseVideos(data)
            }
        } catch(err:Exception){
            err?.printStackTrace()
            Log.d("TypeErr",err?.message ,err.cause)
            Log.d("Error","fallo la coneccion")
        }
        return videos
    }
}
package org.libre.agosto.p2play

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_reproductor.*
import org.libre.agosto.p2play.models.VideoModel

class ReproductorActivity : AppCompatActivity() {
    lateinit var video:VideoModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reproductor)
        ManagerSingleton.context = this

        videoView.settings.javaScriptEnabled = true
        videoView.settings.allowContentAccess = true
        try {
            this.video =  this.intent.extras.getSerializable("video") as VideoModel
            tittleVideoTxt.text = this.video.name
            viewsTxt.text = this.video.views.toString() + ' ' + getString(R.string.view_text)
            userTxt.text = this.video.username
            descriptionVideoTxt.text = this.video.description

            if(this.video.userImageUrl!="")
                Picasso.get().load("https://"+ManagerSingleton.url+this.video.userImageUrl).into(userImg)
            videoView.loadUrl("https://"+ManagerSingleton.url+this.video.embedUrl)
            Log.d("url", videoView.url)
        }
        catch (err:Exception){
            err.printStackTrace()
            Log.d("Error", err?.message)
        }

        subscribeBtn.setOnClickListener { ManagerSingleton.Toast(getString(R.string.comming)) }
    }
}
